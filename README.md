# GGR TIMER

## INSTALL INSTRUCTIONS
Run `npm install`


## RUN TIMER APP
Run `node app.js`

## SERVER / CONTROLLER
To go to the controller timer:
`http://localhost:4200`

## CLIENT 
To go to the client timer:
`http://localhost:4200/client`

## FUN STUFF
You can add a query parameter to change the text color and the background by setting the `c` and the `b` params and use the color hex value *without* the hashtag char `#`.

#### EXAMPLES
To set the color to orange use:
`http://localhost:4200/client?c=ff6900`

To set the background to dark gray use:
`http://localhost:4200/client?b=333333`

To set the color to orange AND the background color to dark gray use:
`http://localhost:4200/client?c=ff6900&b=333333`
