// app.js
var express = require('express');  
var app = express();  
var server = require('http').createServer(app);  
var io = require('socket.io')(server);
var moment = require('moment');
var startTime;
var durationAsSecs = 0;
var serialstate = 0;
var isStarted = false;
/*
var SerialPort = require("serialport");
var port = new SerialPort('/dev/ttyUSB0', {
  baudRate: 9600
});
port.on('open', function(){
  console.log('Serial Port Opend');
  port.on('data', function(data){
    if (data[0] != serialstate) {
      serialstate = data[0];
      switch(serialstate) {
        case 1:
          if (isStarted == true) {
            console.log('stopping timer');
            io.emit('footpedalpressed', {pedal: serialstate, value: 'stop'});
            if (typeof t != "undefined") {
              clearTimeout(t);
              delete t;
              isStarted = false;
            }
          } else {
            io.emit('footpedalpressed', {pedal: serialstate, value: 'start'});
            console.log('starting timer');
            if (typeof t == "undefined") {
              isStarted = true;
              timer();
            }
          }
          break;
        case 2:
          io.emit('footpedalpressed', {pedal: serialstate, value: 'reset'});
          console.log('reset timer');
          if (typeof t != "undefined") {
            clearTimeout(t);
            delete t;
            isStarted = false;
          }
          resetTimer();
          break;
      }
    }
    console.log(data[0]);
  });
});
*/

// settings that is used for both server and client!
var skipHour = true
////////////////////////////////////////////////////

var add = function() {
  var now = moment();
  var duration = moment.duration(now.diff(startTime));
  durationAsSecs = Math.floor(duration.asSeconds());
  io.emit('timer', durationAsSecs);
  timer();
}

var timer = function() {
  t = setTimeout(add, 100);
}
var resetTimer = function() {
  startTime = null;
  durationAsSecs = null;
  io.emit('timer', 0);
}

var setTimer = function(secs) {
  durationAsSecs = secs;
  io.emit('timer', durationAsSecs);
}


/*  EXPRESS FUNCTIONS  */

app.use("/", express.static(__dirname + '/node_modules'));  
app.use("/static", express.static(__dirname + '/static'));  

app.get('/', function(req, res,next) {  
    res.sendFile(__dirname + '/index.html');
});

app.get('/client', function(req, res,next) {  
  res.sendFile(__dirname + '/client.html');
});

io.on('connection', function(client) {  
  console.log('Client connected...');
  client.on('join', function(data) {
    console.log(data);
  });

  client.emit('settings', {skipHour: skipHour});
  client.emit('timer', 0);
  
  client.on('messages', function(data) {
    console.log(data);
    switch(data.message) {
      case 'start':
        if (typeof t == "undefined") {
          if (durationAsSecs != 0) {
            startTime = moment().subtract(durationAsSecs, "s");
          } else {
            startTime = moment();
          }
          isStarted = true;
          timer();
        }
        break;
      case 'stop':
        if (typeof t != "undefined") {
          clearTimeout(t);
          delete t;
          isStarted = false;
        }
        break;
      case 'reset':
        if (typeof t != "undefined") {
          clearTimeout(t);
          delete t;
          isStarted = false;
        }
        resetTimer();
        break;
      case 'set':
        if (typeof t != "undefined") {
          clearTimeout(t);
          delete t;
        }
        setTimer(data.value);
      break;
  }
  });  
});

server.listen(4200);